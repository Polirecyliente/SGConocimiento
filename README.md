# SGConocimiento

SGConocimiento stands for 'Sistema de Gestión del Conocimiento' which is Spanish for 'Knowledge Management System'.

This project is my personal way to manage my knowledge, since I can't remember everything I learn at all times. It's written mainly in English, technical terms are translated to Spanish as well. Given that I'm the only person currently working on this project, excuse some of the cringy commit messages, if any.

If you want to contribute something to this project, please make sure to release your contributions under the MIT license for programming code, and under the Creative Commons BY license for multimedia files. To contribute you can use the Github interface and submit your contributions as a Pull Request.

This project uses Git Large File Storage (Git LFS) to manage non plain text files, such as .png files.