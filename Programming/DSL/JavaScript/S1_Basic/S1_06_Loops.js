/*
    Loops
*/

var iter = 1;
//T// while
while (iter <= 3)
{
    alert(iter);
    iter = iter + 1;
}

//T// for
for (var i = 7; i <= 9; i++)
{
    alert(i);
}