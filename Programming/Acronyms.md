<!--
#   Acronyms

#T# Table of contents

#C# Algorithms
#C# - Cryptography
#C# Languages

#T# Beginning of content
-->

# Algorithms

## Cryptography

DSA: Digital Signature Algorithm, an asymmetric cryptography (public key) algorithm.

ECDSA: Elliptic Curve Digital Signature Algorithm, a variant of the DSA.

EdDSA: Edwards-curve Digital Signature Algorithm, an asymmetric cryptography (public key) algorithm.

RSA: Rivest, Shamir, Adleman, an asymmetric cryptography (public key) algorithm.

# Languages

AST: Abstract Syntax Tree, a tree diagram to show the syntactic elements in a given source code.

DOM: Document Object Model, in a markup language, the DOM is the set of tags and their hierarchical structure 